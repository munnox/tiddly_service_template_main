/*\
title: $:/plugins/munnox/external-interface/Widgets/ext-test-widget.js
type: application/javascript
tags: My/plugins/munnox/external_interface
module-type: widget

External test widget

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;
const axios = require('$:/plugins/munnox/external-interface/js/library/axios.min.js');


var MyWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
MyWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
MyWidget.prototype.render = function(parent,nextSibling) {
	this.parentDomNode = parent;
	this.computeAttributes();
	this.execute();
	console.log("variables:");
	console.log(this);
  var text = "Hello, World! " + this.message + " edited!" + this.wiki.getTiddlerText("$:/plugins/munnox/external_interface/Readme", "<empty>")
	console.log(text);
	var textNode = this.document.createTextNode(text);
	parent.insertBefore(textNode,nextSibling);
	// Make a request for a user with a given ID
	axios.get('/robert/dev/')
		.then(function (response) {
			// handle success
			//console.log(response);
		})
		.catch(function (error) {
			// handle error
			console.log(error);
		})
		.then(function () {
			// always executed
		});
	this.domNodes.push(textNode);
  var result = this.wiki.filterTiddlers(this.filter,this);
	console.log("filter:", this.filter)
	console.log(result);
};

MyWidget.prototype.execute = function(){
	console.log("Running testing execute...");
	var defaultFilter = "[!is[system]sort[title]]";
	this.message = this.getAttribute("$$message","debug");
	this.logAll = this.getAttribute("$$all","no") === "yes" ? true : false;
	this.filter = this.getAttribute("$$filter", defaultFilter);
}

MyWidget.prototype.refresh = function(changedTiddlers) {
  // Regenerate and rerender the widget and
  // replace the existing DOM node
  console.log("ext refresh");
	console.log(changedTiddlers);
  this.refreshSelf();
  return true;
};

exports["ext-test-widget"] = MyWidget;

})();