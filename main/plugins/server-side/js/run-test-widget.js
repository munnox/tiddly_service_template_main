/*\
title: $:/plugins/munnox/server-side/Widgets/run-test-widget.js
type: application/javascript
tags: My/plugins/munnox/external_interface
module-type: widget

External test widget

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;
// const axios = require('$:/plugins/munnox/external_interface/library/axios.min.js');


var MyWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
MyWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
MyWidget.prototype.render = function(parent,nextSibling) {
	this.parentDomNode = parent;
	this.computeAttributes();
	this.execute();
	console.log("run-test-widget");
	console.log("variables:");
	console.log(this);
	var tiddler_text = this.wiki.getTiddlerText("$:/plugins/munnox/server-side/readme", "<empty>");
	var text = "Hello, World! " + this.message + " edited!" + tiddler_text;
	// console.log(text);
	var textNode = this.document.createTextNode(text);
	parent.insertBefore(textNode,nextSibling);
	this.domNodes.push(textNode);

	// Make a request for a user with a given ID
	// axios.get('/robert/dev/')
	// 	.then(function (response) {
	// 		// handle success
	// 		//console.log(response);
	// 	})
	// 	.catch(function (error) {
	// 		// handle error
	// 		console.log(error);
	// 	})
	// 	.then(function () {
	// 		// always executed
	// 	});
	var wid = this;
	$tw.utils.httpRequest({
		url: "/recipes/default/server_run/testing_message",
		type: "GET",
		// headers: headers,
		callback: function(err,data,xhr) {
			if(err) {
				return;
			}
			console.log("returned")
			console.log(data)
			console.log(xhr)
			var text = xhr.response;
			var result = JSON.parse(text);


			var codeNode = wid.document.createElement("code"),
				domNode = wid.document.createElement("pre");
			codeNode.appendChild(wid.document.createTextNode(result.stdout));
			domNode.appendChild(codeNode);
			parent.insertBefore(domNode,nextSibling);
			wid.domNodes.push(domNode);

			var codeNode = wid.document.createElement("code"),
				domNode = wid.document.createElement("pre");
			codeNode.appendChild(wid.document.createTextNode(result.stderr));
			domNode.appendChild(codeNode);
			parent.insertBefore(domNode,nextSibling);
			wid.domNodes.push(domNode);
			// var host = xhr.getResponseHeader("host");
		}
	});


  var result = this.wiki.filterTiddlers(this.filter,this);
	console.log("filter:", this.filter)
	console.log(result);
};

MyWidget.prototype.execute = function(){
	var defaultFilter = "[!is[system]sort[title]]";
	this.message = this.getAttribute("$$message","debug");
	// this.logAll = this.getAttribute("$$all","no") === "yes" ? true : false;
	this.filter = this.getAttribute("$$filter", defaultFilter);
	console.log(`Running server execute... ${this.message}, ${this.filter}`);
}

MyWidget.prototype.refresh = function(changedTiddlers) {
  // Regenerate and rerender the widget and
  // replace the existing DOM node
  console.log("ext refresh");
	console.log(changedTiddlers);
  this.refreshSelf();
  return true;
};

exports["run-test-widget"] = MyWidget;

})();