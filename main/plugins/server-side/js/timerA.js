/*\
title: $:/plugins/munnox/server-side/timer/timerA.js
type: application/javascript
tags: My/plugins/munnox/external_interface
module-type: library

Timer Functions

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Timer = {
	"name": "TimerA"
}

Timer.execute = function(context){
	// var $tw = context.$tw;
	// console.log("Print TimerA: " + JSON.stringify(context) );
	var tiddler = $tw.wiki.getTiddler(context.timer_tiddler);

	var title = "results-TimerA";
	var tid = {
		"text": `TimerA\n\n[[$:/plugins/munnox/server-side/timer/timerA.js]]\n\nInterval: ${context.interval}\n\nTimer run count: ${context.count}\n\nContext:\n\n\`\`\`\n${JSON.stringify(context, null, 2)}\n\`\`\``
	};
	$tw.wiki.addTiddler(new $tw.Tiddler(tid,{title: title}));

	console.log(`Timer '${context.timer_tiddler}' (${context.count}) Tiggered: ${tiddler.fields.text.length}`);
}

exports["Timer"] = Timer;

})();
