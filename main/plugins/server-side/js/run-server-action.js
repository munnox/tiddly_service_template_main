/*\
title: $:/plugins/munnox/server-side/Widgets/run-server-action.js
type: application/javascript
tags: My/plugins/munnox/external_interface
module-type: widget

Action widget to log debug messages

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var RunServerProcessWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
RunServerProcessWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
RunServerProcessWidget.prototype.render = function(parent,nextSibling) {
	this.computeAttributes();
	this.execute();
};

RunServerProcessWidget.prototype.execute = function(){
	// this.message = this.getAttribute("$$message","debug");
	// this.logAll = this.getAttribute("$$all","no") === "yes" ? true : false;
	this.command = this.getAttribute("$$command");
}

/*
Refresh the widget by ensuring our attributes are up to date
*/
RunServerProcessWidget.prototype.refresh = function(changedTiddlers) {
	this.refreshSelf();
	return true;
};

/*
Invoke the action associated with this widget
*/
RunServerProcessWidget.prototype.invokeAction = function(triggeringWidget,event) {
	this.log();

	// var tiddler = $tw.wiki.getTiddler("command");
	// const command = tiddler.fields.text;

	var wid = this;
	$tw.utils.httpRequest({
		url: "/recipes/default/server_run/saved_tiddler/" + encodeURIComponent(this.command),
		type: "GET",
		// headers: headers,
		callback: function(err,data,xhr) {
			console.log("returned");
			console.log(err);
			console.log(xhr);
			if(err) {
				var result = {
					"error": "Error\n\n```\n" + `${JSON.stringify(err, null, 2)}\n` + "```\n"
				};
			}
			else {
				// console.log("returned")
				// console.log(data)
				// console.log(xhr)
				var text = xhr.response;
				try {
					var result = JSON.parse(text);
				}
				catch {
					var result = {
						"error": "Result NOT a Parseable JSON"
					};
				}

			}

			var title = "results-local";
			var tid = {
				"text": "Command:\n\n"
						+ "```\n"
						+ `${wid.command}\n`
						+ "```\n"
						// + `State:\n${JSON.stringify(state)}\n\n`
						+ "\n"
						+ "STDOUT:\n\n"
						+ "```\n"
						+ `${result.stdout}\n`
						+ "```\n"
						+ "\n"
						+ "STDERR:\n\n"
						+ "```\n"
						+ `${result.stderr}\n`
						+ "```\n"
						+ "\n"
						+ "Error Message:\n\n"
						+ "```\n"
						+ `${result.error}\n`
						+ "```\n"
						+ "Result  object keys:\n\n"
						+ "```\n"
						+ `${JSON.stringify(Object.keys(result), null, 2)}\n`
						+ "```\n"
						+ "Full Message:\n\n"
						+ "```\n"
						+ `${JSON.stringify(result, null, 2)}\n`
						+ "```\n"
			};

			$tw.wiki.addTiddler(new $tw.Tiddler(tid,{title: title}));
			console.log("tiddler saved");
			// var codeNode = wid.document.createElement("code"),
			// 	domNode = wid.document.createElement("pre");
			// codeNode.appendChild(wid.document.createTextNode(text));
			// domNode.appendChild(codeNode);
			// parent.insertBefore(domNode,nextSibling);
			// wid.domNodes.push(domNode);
			// var host = xhr.getResponseHeader("host");
		}
	});
	return true; // Action was invoked
};

RunServerProcessWidget.prototype.log = function() {
	var data = {},
		dataCount,
		allVars = {},
		filteredVars;

	$tw.utils.each(this.attributes,function(attribute,name) {
		if(name.substring(0,2) !== "$$") {
			data[name] = attribute;
		}
	});

	for(var v in this.variables) {
		allVars[v] = this.getVariable(v,{defaultValue:""});
	}
	if(this.filter) {
		filteredVars = this.wiki.compileFilter(this.filter).call(this.wiki,this.wiki.makeTiddlerIterator(allVars));
		$tw.utils.each(filteredVars,function(name) {
			data[name] = allVars[name];
		});
	}
	dataCount = $tw.utils.count(data);

	console.group(this.message);
	if(dataCount > 0) {
		$tw.utils.logTable(data);
	}
	if(this.logAll || !dataCount) {
		console.groupCollapsed("All variables");
		$tw.utils.logTable(allVars);
		console.groupEnd();
	}
	console.groupEnd();
}

exports["run-server-action"] = RunServerProcessWidget;

})();
