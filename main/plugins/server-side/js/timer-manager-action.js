/*\
title: $:/plugins/munnox/server-side/Widgets/timer-manager-action.js
type: application/javascript
tags: My/plugins/munnox/external_interface
module-type: widget

Action widget to start a server side timer

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var TimerManagerWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
TimerManagerWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
TimerManagerWidget.prototype.render = function(parent,nextSibling) {
	this.computeAttributes();
	this.execute();
};

TimerManagerWidget.prototype.execute = function(){
	this.name = this.getAttribute("$$name","timer1");
	this.action = this.getAttribute("$$action","null");
	this.interval = this.getAttribute("$$interval","5000");
	this.message = this.getAttribute("$$message","debug");
}

/*
Refresh the widget by ensuring our attributes are up to date
*/
TimerManagerWidget.prototype.refresh = function(changedTiddlers) {
	this.refreshSelf();
	return true;
};

/*
Invoke the action associated with this widget
*/
TimerManagerWidget.prototype.invokeAction = function(triggeringWidget,event) {
	console.log(`Invoke Timer ${this.name} ${this.action}`);

	// var tiddler = $tw.wiki.getTiddler("command");
	// const command = tiddler.fields.text;

	if ((this.action == 'start' || this.action == 'stop' || this.action == 'list') ) {
		console.log("Action Known: " + this.action)
	}
	else {
		console.log("Unknown action: " + this.action)
		return true;
	}

	var args = this.name + "::" + this.action;

	if (this.action == 'start') {
		try {
			const interval = parseInt(this.interval);
			if (interval < 5000 || interval > 60000) {
				console.log("Interval outside accepted range");
				return true
			}
		} catch {
			console.log(`Unknown Interval given :${this.interval} needs to be a parsable int`)
			return true
		}
		args = args + "::" + this.interval;
	}

	var wid = this;
	$tw.utils.httpRequest({
		url: "/recipes/default/server_timer/" + encodeURIComponent(args),
		type: "GET",
		// headers: headers,
		callback: function(err,data,xhr) {
			if(err) {
				return;
			}
			console.log("Timer manager returned")
			console.log(data)
			// console.log(xhr)
			// var data = xhr.data;
			var text = xhr.response;
			var code_str = '```';
			var title = "results-timer";
			var tid = {
				"text": (`
				Message:
				
				${code_str}javascript
				${JSON.stringify(text, null, 2)}
				${code_str}
				
				Data:
				
				${code_str}javascript
				${JSON.stringify(data, null, 2)}
				${code_str}
				`).replace(/^ +/g, '')
			};

			$tw.wiki.addTiddler(new $tw.Tiddler(tid,{title: title}));
			console.log("timer started");

			// var codeNode = wid.document.createElement("code"),
			// 	domNode = wid.document.createElement("pre");
			// codeNode.appendChild(wid.document.createTextNode(text));
			// domNode.appendChild(codeNode);
			// parent.insertBefore(domNode,nextSibling);
			// wid.domNodes.push(domNode);
			// var host = xhr.getResponseHeader("host");
		}
	});
	return true; // Action was invoked
};


exports["timer-manager-action"] = TimerManagerWidget;

})();
