/*\
title: $:/core/modules/server/routes/server_timer.js
type: application/javascript
module-type: route

GET /recipes/default/server_timer/:message

\*/
(function() {

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

// setInterval(myMethod, 5000);
function runner_method_closure(timer_name, interval) {

	var timer_vars = {
		// "$tw": $tw,
		"count": 0,
		"timer_tiddler": timer_name,
		"interval": interval
	};
	console.log(`Timer name: '${timer_name}'`);
	var timer_obj = require(timer_name).Timer;
	console.log(timer_obj);

	function runner_method() {
		//this will repeat every interval seconds
		timer_obj.execute(timer_vars);

		// You can reset counter here
		// var tiddler = $tw.wiki.getTiddler(timer_name + '.js');
		// console.log(`Timer '${timer_name}' (${timer_vars.count}) Tiggered: ${tiddler.fields.text}`);
		timer_vars.count = timer_vars.count + 1;
	}

	return runner_method

}


$tw.wiki.addTimer = function(name,timer) {
	this.timers = this.timers || {};
	// this.timers[name] = this.timers[name]  || [];
	// this.timers[name].push(timer);
	this.timers[name] = timer;
};

$tw.wiki.removeTimer = function(name,timer) {
	var timer = this.timers[name];
	if(timer) {
		delete this.timers[name];
	}
};

exports.method = "GET";

exports.path = /^\/recipes\/default\/server_timer\/(.+)$/;

// Designed to be start and stop timer trigger service
// Satifies the route template and so is loaded and runs server side.
// The message can be start and stop to control the timer.
// Only one time at present.
// var timer = null;

// $tw.wiki.addTimer = exports.addTimer;
// $tw.wiki.removeTimer = exports.removeTimer;

exports.handler = function(request,response,state) {
	console.log("Running Server Timer manager");

	var message = $tw.utils.decodeURIComponentSafe(state.params[0]);
	var parts = message.split("::")
	console.log(`path: ${message} ${parts}`);
	var timer_name = parts[0] || "simple";
	var action = parts[1] || "null";
	var timer_interval = parseInt(parts[2] || "5000");
	var results = null;

	if ((action == 'start' || action == 'stop' || action == 'list') ) {
		console.log("Action Known: " + action)
	}
	else {
		console.log("Unknown action: " + action)
		return true;
	}

	if (action == "start") {
		var timer = setInterval(runner_method_closure(timer_name, timer_interval), timer_interval);
		$tw.wiki.addTimer(timer_name, timer)
	}
	if (action == "stop") {
		var timer = $tw.wiki.timers[timer_name];
		clearInterval(timer)
		$tw.wiki.removeTimer(timer_name);
	}
	if (action == "list") {
		var timers = $tw.wiki.timers || {};
		results = Object.keys(timers);
	}

	state.sendResponse(
		200,
		{"Content-Type": "application/json"},
		JSON.stringify({
			"message": `Interval timer: ${timer_name} - ${timer}`,
			"results": results
		}),
		"utf8"
	);
};

}());
