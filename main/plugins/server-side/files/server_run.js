/*\
title: $:/core/modules/server/routes/server_run.js
type: application/javascript
module-type: route

GET /recipes/default/server_run/:message

\*/
(function() {

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

exports.method = "GET";

exports.path = /^\/recipes\/default\/server_run\/saved_tiddler\/(.+)$/;

var exec = require('child_process').exec;

function executeProcess(command, callback) {
  exec(command, function (err, stdout, stderr) {
    console.log("Running Process command: " + command);
    callback(err, stdout, stderr)
  });
}

exports.handler = function(request, response, state) {

	var cmd_tiddler = $tw.utils.decodeURIComponentSafe(state.params[0]);
	var data = $tw.utils.parseJSONSafe(state.data);
	var tiddler = state.wiki.getTiddler(cmd_tiddler);
	const command = tiddler.fields.text;
	
	var title = "results";
	var tid = {
		"text": `Command:\n${command}\n\n`
				// + `State:\n${JSON.stringify(state)}\n\n`
				+ `Tiddler:\n${JSON.stringify(tiddler)}`
	};
	
	// console.log(state)
	// console.log(`tiddler: ${JSON.stringify(tiddler)}`);
	// var headers = {
	// 	Accept: "*/*;charset=UTF-8"
	// };
	// $tw.utils.httpRequest({
	// 	url: self.uri(),
	// 	type: "HEAD",
	// 	headers: headers,
	// 	callback: function(err,data,xhr) {
	// 		if(err) {
	// 			return;
	// 		}
	// 		var etag = xhr.getResponseHeader("ETag");
	// 		if(!etag) {
	// 			return;
	// 		}
	// 		self.etag = etag.replace(/^W\//,"");
	// 	}
	// });
  	// const command = `git status \"${ '-v'}\"`;
	executeProcess(
		command,
		function(err, stdout, stderr) {
		// console.log(`Job run ${err} ${stdout} ${stderr}`);
		var tid = {
			"text": "Command:\n" + `${command}` + "\n\n"
					+ "stdout:\n\n```\n"
					+ `${stdout}\n`
					+ "```\n\n"
					+ "stderr:\n\n```\n"
					+ `${stderr}\n`
					+ "```\n\n"
					+ "Error:\n\n```\n"
					+ `${JSON.stringify(err, null, 2)}\n`
					+ "```\n\n"
					+ "Tiddler:\n\n```json\n"
					+ `${JSON.stringify(tiddler, null, 2)}\n`
					+ "```\n"
					// + `State:\n\n\`\`\`json\n${JSON.stringify(state, null, 2)}\n\`\`\`\n\n`
		};
		state.wiki.addTiddler(new $tw.Tiddler(tid,{title: title}));
		state.sendResponse(
			200,
			{"Content-Type": "application/json"},
			JSON.stringify(
				{
					'cmd_tiddler': `${cmd_tiddler}`,
					'error':`${err}`,
					'stdout': `${stdout}`,
					'stderr': `${stderr}`
				}
			),
			"utf8"
		);
	});
};

}());
